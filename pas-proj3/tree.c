#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include "defs.h"
#include "types.h"
#include "tree.h"
#include "symtab.h"
#include "message.h"
#include "encode.h"
#include <string.h>

//For constant folding
static const unsigned long MAX_UCHAR = (unsigned long)(unsigned char)(-1);
static int get_single_char(const char * str)
{
    if (*str == '\0') return(-1);
    if (*str != '\\') {
  if (*(str+1) == '\0') return((int)(unsigned char)(*str));
  return(-1);
    }

    /* str starts with an escape char ("\"); check for legal escape seqs */
    /* single quotes need not be escaped in C strings. */
    switch (*++str) {
    case 'n': return '\n';
    case 't': return '\t';
    case 'b': return '\b';
    case 'r': return '\r';
    case 'f': return '\f';
    case 'v': return '\v';
    case '\\': return '\\';
    case '"': return '"';
    case 'a': return '\a';
    case '?': return '?';
    }

    /* str isn't a character escape.  Check for octal or hex escape codes */
    if (*str == 'x') {  /* if hexadecimal */
  long num;
  char *end;
  num = strtol(++str, &end, 16);
  if (*end != '\0' || (unsigned long)num > MAX_UCHAR)
      return(-1);
  return((int)num);
    }

    if (*str >= '0' && *str < '8') {  /* if octal */
  long num;
  char *end;
  num = strtol(str, &end, 8);
  if (*end != '\0' || end-str > 3 || (unsigned long)num > MAX_UCHAR)
      return(-1);
  return((int)num);
    }

    return(-1);
}
void create_gdecl(VAR_ID_LIST list,TYPE type)
{
    if (ty_query(type) == TYFUNC)
    {
        error("Variable(s) must be of data type");
    }

    ST_DR new_data_rec;

    while(list!=NULL)
    {
        resolve_all_ptr();
        new_data_rec = stdr_alloc();
        // Fill the fields of the new symbol table data record
        new_data_rec->tag=GDECL;
        new_data_rec->u.decl.type=type;
        new_data_rec->u.decl.sc=NO_SC;
        new_data_rec->u.decl.is_ref=FALSE;
        new_data_rec->u.decl.err = (ty_query(type) == TYERROR ? TRUE : FALSE);

        if (!st_install(list->id,new_data_rec))
        {
            error("Duplicate variable declaration: \"%s\"", st_get_id_str(list->id));
            free(new_data_rec);
        }

        list=list->next;
    }
}

void create_typename(ST_ID id,TYPE new_type)
{
    ST_DR new_data_rec;
    new_data_rec= stdr_alloc();

    //Fill the fields of the new symbol table data record
    new_data_rec->tag=TYPENAME;
    new_data_rec->u.typename.type=new_type;

    if (!st_install(id,new_data_rec))
    {
        error("This Type name is already installed");
    }

}

/* Is the ST_ID already installed in the symbol table as type*/
TYPE check_typename(ST_ID id)
{
    ST_DR chcktype;
    int chck;

    if (id == NULL)
        bug("null id passed to \"st_install\"");

    if ((chcktype = st_lookup(id,&chck)) == NULL)
    {
        error("Undeclared type name: \"%s\"", st_get_id_str(id));
        return ty_build_basic(TYERROR);
    }

    return chcktype->u.typename.type;

}

/* Checks function return type whether it is simple or not*/
TYPE check_function_type(TYPE type)
{
    if (ty_query(type) == TYERROR)
    {
        error("Error in the function return type");
        return ty_build_basic(TYERROR);
    }

    if (ty_query(type) == TYFUNC)
    {
        error("Function return type must be simple type");
        return type;
    }

    if (ty_query(type) == TYARRAY)
    {
        error("Function return type must be simple type");
        return type;
    }

    return type;
}

/* Checks an array to see if it's element type is valid */
TYPE check_array(TYPE type, INDEX_LIST i)
{
    if (ty_query(type) == TYERROR)
    {
        error("Data type expected for array elements");
        return ty_build_basic(TYERROR);
    }
    if (ty_query(type) == TYFUNC)
    {
        error("Data type expected for array elements");
        error("Variable(s) must be of data type");
        return ty_build_basic(TYERROR);
    }


    return ty_build_array(type, i);
}

PARAM_LIST check_param(PARAM_LIST p)
{

    if (!p) bug("%s:%d check_params received a NULL pointer\n", __FILE__, __LINE__);
    if (!p->id) bug("%s:%d check_params received a pointer to NULL id\n", __FILE__, __LINE__);

    if (ty_query(p->type) == TYARRAY || ty_query(p->type) == TYFUNC)
    {
        error("Parameter type must be a simple type");
    }

    PARAM_LIST c = p->next;
    while (c)
    {
        if (!strcmp(st_get_id_str(p->id), st_get_id_str(c->id)))
        {
            // ids are identical, return null instead of duplicate
            error("Duplicate parameter name: \"%s\"", st_get_id_str(p->id));
        }
        if (ty_query(c->type) == TYARRAY || ty_query(c->type) == TYFUNC)
        {
            error("Parameter type must be a simple type");
        }
        c = c->next;
    }

    return p;
}

VAR_ID_LIST create_variable_list (VAR_ID_LIST list,ST_ID id)
{
    VAR_ID_LIST id_list, temp_list;
    id_list = (VAR_ID_LIST)malloc(sizeof(VAR_ID));
    id_list->id = id;
    id_list->next=list;
    return id_list;
}

PARAM_LIST create_param_list(VAR_ID_LIST id_list,TYPE type,BOOLEAN value)
{
    VAR_ID_LIST id_ptr = id_list;
    PARAM_LIST head = NULL;
    PARAM_LIST next;

    while(id_ptr != NULL)
    {
        next = (PARAM_LIST) malloc(sizeof(PARAM));
        next->id = id_ptr->id;
        next->type = type;
        next->sc = NO_SC;
        next->err = FALSE;
        next->is_ref = value;
        next->prev = NULL;
        next->next = NULL;
        next->next=head;
        head=next;
        id_ptr = id_ptr->next;
    }

    return next;
}

/* Concats two parameter lists*/
PARAM_LIST concat_param_list (PARAM_LIST list1,PARAM_LIST list2)
{
    if (!list1 && !list2) return NULL;
    if (!list1) return list2;
    if (!list2) return list1;


    PARAM_LIST new_list;
    new_list=list1;
    while (new_list->next!=NULL)
    {
        new_list=new_list->next;
    }
    new_list->next=list2;
    return list1;
}

/* Concats index lists */
INDEX_LIST concat_index_lists (INDEX_LIST list1,TYPE type)
{
    INDEX_LIST ptr, list2;

    list2 = (INDEX_LIST) malloc(sizeof(INDEX));
    list2 = create_list_from_type(type);

    ptr=list1;
    while (ptr->next)
    {
        ptr = ptr->next;
    }
    ptr->next = list2;
    return list1;

}

/* Creates an index list for array*/
INDEX_LIST create_list_from_type(TYPE type)
{
    if (ty_query(type) == TYERROR) return NULL;
    INDEX_LIST index;
    index=(INDEX_LIST) malloc(sizeof(INDEX));
    index->type=type;
    index->next=NULL;
    index->prev=NULL;
    return index;
}

/* changed in this part of the project to take expr args*/
TYPE check_subrange(EXPR low, EXPR high)
{
    long i, j;
    if (low->tag != INTCONST || high->tag != INTCONST)
    {
        error("Subrange indexs are not Integers");
        return ty_build_basic(TYERROR);
    }

    i = low->u.intval;
    j = high->u.intval;
    if (i < j)
    {
        return ty_build_subrange(ty_build_basic(TYSIGNEDLONGINT), i, j);
    }
    error("Empty subrange in array index");
    error("Illegal index type (ignored)");
    return ty_build_basic(TYERROR);
}

/* fixing pointer resolving in this part by using TYPE_LIST */
static TYPE_LIST UNRESOLVED_POINTERS = NULL;
TYPE build_unresolved_pointer(TYPE ret_type, TYPE object)
{
    TYPE_LIST new_unresolved_pointer;
    new_unresolved_pointer = (TYPE_LIST) malloc(sizeof(TLIST_NODE));
    if (object == NULL)
    {
        new_unresolved_pointer->type = ret_type;
        new_unresolved_pointer->next = UNRESOLVED_POINTERS;
        UNRESOLVED_POINTERS = new_unresolved_pointer;
    }
    else
        new_unresolved_pointer->next = NULL;
    return ret_type;
}


/* Traverses the linked list and repeatedly resolves the pointers */
void resolve_all_ptr()
{
    int block;
    ST_ID id;
    ST_DR data_rec;
    TYPE_LIST unresolved = UNRESOLVED_POINTERS;
    UNRESOLVED_POINTERS = NULL;

    while(unresolved!=NULL)
    {
        ty_query_ptr(unresolved->type, &id);
        data_rec = st_lookup(id,&block);
        if (data_rec == NULL)
        {
            error("Unresolved type name: \"%s\"", st_get_id_str(id));
            unresolved = unresolved->next;
            continue;
        }
        if (data_rec->tag == TYPENAME)
        {
            if(!ty_resolve_ptr(unresolved->type, data_rec->u.typename.type))
                error("Unresolved type name: \"%s\"", st_get_id_str(id));
        }
        else
        {
            error("Unidentified type tag\n");
        }
        unresolved=unresolved->next;
    }
}





/**** ----------------  START OF CODE: PART 2  ------------------------ ****/

void enter_main_body()
{
    b_func_prologue("main");
}

void exit_main_body()
{
    b_func_epilogue("main");
}



EXPR_LIST expr_prepend(EXPR expr, EXPR_LIST list)
{
    EXPR_LIST alt_list;
    alt_list = (EXPR_LIST)malloc(sizeof(EXPR_LIST_NODE));

    alt_list->expr = expr;
    alt_list->next = NULL;

    if (list != NULL)
    {
        alt_list->next = list;
    }

    return alt_list;
}

EXPR_LIST expr_list_reverse(EXPR_LIST list)
{
    EXPR_LIST tmp;
    EXPR_LIST prev = NULL;

    while (list != NULL)
    {
        tmp = list->next;
        list->next = prev;
        prev = list;
        list = tmp;
    }

    return prev;
}

void expr_free(EXPR expr)
{

    if (expr != NULL)
    {
        expr_free(expr);
        free(expr);
    }

}

void expr_list_free(EXPR_LIST list)
{

    if (list != NULL)
    {
        expr_list_free(list);
        free(list);
    }

}


EXPR make_id_expr(ST_ID id)
{
    int block;
    EXPR ret;
    ret = (EXPR)malloc(sizeof(EXPR_NODE));
    assert(ret != NULL);

    ST_DR rec = st_lookup(id, &block);
    if (rec == NULL)
    {
        error("Undeclared identifier \"%s\" in expression", st_get_id_str(id));
        return make_error_expr();
    }
    if (rec->tag == TYPENAME)
    {
        error("Identifier \"%s\" installed as TYPENAME", st_get_id_str(id));
        return make_error_expr();
    }

    ret->type = rec->u.decl.type;
    switch(rec->tag)
    {
    case GDECL:
        ret->tag = GID;
        ret->u.gid = id;
        break;
    case LDECL:
    case PDECL:
        ret->tag = LVAR;
        ret->u.lvar.is_ref = rec->u.decl.is_ref;
        ret->u.lvar.link_count = st_get_cur_block() - block;
        ret->u.lvar.offset = rec->u.decl.v.offset;
        break;
    case FDECL:
        if (block <= 1)
        {
            ret->tag = GID;
            ret->u.gid = id;
        }
        else
        {
            ret->tag = LFUN;
            ret->u.lfun.global_name = rec->u.decl.v.global_func_name;
            ret->u.lfun.link_count = st_get_cur_block() - block;
        }
        break;
    default:
        break;
    }
    return ret;
}

EXPR make_intconst_expr(long val, TYPE type)
{
    EXPR ret;
    ret = (EXPR)malloc(sizeof(EXPR_NODE));
    assert(ret != NULL);
    ret->tag = INTCONST;
    ret->type = type;
    ret->u.intval = val;
    return ret;
}

EXPR make_realconst_expr(double val)
{
    EXPR ret;
    ret = (EXPR)malloc(sizeof(EXPR_NODE));
    assert(ret != NULL);
    ret->tag = REALCONST;
    ret->type = ty_build_basic(TYDOUBLE);
    ret->u.realval = val;
    return ret;
}

EXPR make_strconst_expr(char *str)
{
    EXPR ret;
    ret = (EXPR)malloc(sizeof(EXPR_NODE));
    assert(ret != NULL);
    ret->tag = STRCONST;
    ret->type = ty_build_ptr(ty_build_basic(TYUNSIGNEDCHAR));
    ret->u.strval = str;
    return ret;
}

EXPR make_null_expr(EXPR_NULLOP op)
{
    EXPR ret;
    ret = (EXPR)malloc(sizeof(EXPR_NODE));
    assert(ret != NULL);
    ret->tag = NULLOP;
    ret->u.nullop.op = op;

    if (op == NIL_OP)
    {
        ret->type = ty_build_basic(TYVOID);
    }
    else if (op == NULL_EOF_OP || op == NULL_EOLN_OP)
    {
        ret->type = ty_build_basic(TYSIGNEDCHAR);
    }
    return ret;
}

static int getSpecialCharacter(const char * str)
{
    if (*str == '\0') return(-1);
    if (*str != '\\')
    {
        if (*(str+1) == '\0') return((int)(unsigned char)(*str));
        return(-1);
    }

    switch (*++str) //escaped chars
    {
    case 'n':
        return '\n';
    case 't':
        return '\t';
    case 'b':
        return '\b';
    case 'r':
        return '\r';
    case 'f':
        return '\f';
    case 'v':
        return '\v';
    case '\\':
        return '\\';
    case '"':
        return '"';
    case 'a':
        return '\a';
    case '?':
        return '?';
    }

    if (*str == 'x') // hexa
    {
        long num;
        char *end;
        num = strtol(++str, &end, 16);
        if (*end != '\0' || (unsigned long)num > ((unsigned long)(unsigned char)(-1)))
            return(-1);
        return((int)num);
    }

    if (*str >= '0' && *str < '8') // octal
    {
        long num;
        char *end;
        num = strtol(str, &end, 8);
        if (*end != '\0' || end-str > 3 || (unsigned long)num > ((unsigned long)(unsigned char)(-1)))
            return(-1);
        return((int)num);
    }

    return(-1);
}

EXPR make_un_expr(EXPR_UNOP op, EXPR sub)
{
    EXPR ret;
    ret = (EXPR)malloc(sizeof(EXPR_NODE));
    assert(ret != NULL);
    ret->tag = UNOP;
    ret->type = sub->type;
    ret->u.unop.op = op;
    ret->u.unop.operand = sub;
    TYPETAG sub_tag = ty_query(sub->type);
    ST_ID id;
    TYPE base_type,next;
    long low, high;

    if (op == DEREF_OP)
    {
        return ret;
    }

    if (op == ADDRESS_OP || op == NEW_OP)
    {
        if (is_lval(sub) == FALSE)
        {
            return make_error_expr();
        }
    }
    else
    {
        if (is_lval(sub) == TRUE)
        {
            ret->u.unop.operand = make_un_expr(DEREF_OP,sub);
        }
    }

    sub_tag = ty_query(sub->type);
    if (is_lval(sub) == FALSE)
    {
        if (sub_tag == TYFLOAT)
        {
            EXPR convertedNode = make_un_expr(CONVERT_OP, sub);
            convertedNode->type = ty_build_basic(TYDOUBLE);
            ret->u.unop.operand = convertedNode;
        }
        else if (sub_tag == TYSUBRANGE)
        {
            EXPR convertedNode = make_un_expr(CONVERT_OP, sub);
            base_type = ty_query_subrange(sub->type, &low, &high);
            convertedNode->type = base_type;
            ret->u.unop.operand = convertedNode;
        }
    }

    sub_tag = ty_query(sub->type);
    if(op == NEG_OP)
    {
        if (sub_tag != TYSIGNEDLONGINT &&  sub_tag != TYDOUBLE)
        {
            error("Incorrect type in NEG_OP");
            return make_error_expr();
        }
    }
    else if(op == ORD_OP)
    {
        if (sub_tag != TYUNSIGNEDCHAR && sub_tag != TYSIGNEDCHAR  && sub_tag != TYPTR)
        {
            error("Incorrect type in ORD_OP");
            return make_error_expr();
        }
        if(sub_tag == TYPTR)
        {
            sub->tag = INTCONST;
            sub->u.intval = getSpecialCharacter(sub->u.strval);
        }
        ret->type = ty_build_basic(TYSIGNEDLONGINT);
    }
    else if(op == CHR_OP)
    {
        if (sub_tag != TYSIGNEDLONGINT && sub_tag != TYDOUBLE )
        {
            error("Incorrect type in CHR_OP");
            return make_error_expr();
        }
        ret->type = ty_build_basic(TYUNSIGNEDCHAR);
    }
    else if(op == SUCC_OP)
    {
        if (sub_tag != TYSIGNEDLONGINT && sub_tag != TYDOUBLE && sub_tag != TYUNSIGNEDCHAR)
        {
            error("Incorrect type in SUCC_OP");
            return make_error_expr();
        }
    }
    else if(op == PRED_OP)
    {
        if (sub_tag != TYSIGNEDLONGINT && sub_tag != TYDOUBLE && sub_tag != TYUNSIGNEDCHAR)
        {
            error("Incorrect type in PRED_OP");
            return make_error_expr();
        }
    }
    else if(op == PLUS_OP)
    {
        if (sub_tag != TYSIGNEDLONGINT && sub_tag != TYDOUBLE)
        {
            error("Incorrect type in UPLUS_OP");
            return make_error_expr();
        }
    }
    else if(op == INDIR_OP)
    {
        ret->type = ty_query_ptr(sub->type, &id);
    }

    return ret;
}

EXPR make_bin_expr(EXPR_BINOP op, EXPR left, EXPR right)
{
    TYPETAG left_type = ty_query(left->type);
    TYPETAG right_type = ty_query(right->type);
    long low, high;
    TYPE base_type;

    if (left->tag == ERROR || right->tag == ERROR)
    {
        return make_error_expr();
    }

    EXPR ret = (EXPR)malloc(sizeof(EXPR_NODE));
    assert(ret != NULL);

    ret->tag = BINOP;
    ret->u.binop.op = op;
    ret->u.binop.left = left;
    ret->u.binop.right = right;
    ret->type = left->type;

    if (op == ASSIGN_OP)
    {
        if(right->tag == STRCONST && ty_query(left->type)==TYUNSIGNEDCHAR)
        {
            right->tag = INTCONST;
            right->u.intval = getSpecialCharacter(right->u.strval);
        }
        if (is_lval(left) == FALSE)
        {
            error("Assignment requires l-value on the left");
            return make_error_expr();
        }
        if (right_type == TYVOID || right_type == TYFUNC || right_type == TYERROR)
        {
            error("Cannot convert between nondata types");
            return make_error_expr();
        }
        else if ((left_type == TYUNSIGNEDLONGINT || left_type == TYSIGNEDLONGINT) && (right_type == TYFLOAT || right_type == TYDOUBLE || right_type == TYUNSIGNEDCHAR))
        {
            error("Illegal conversion");
            return make_error_expr();
        }
        else if((left_type == TYUNSIGNEDCHAR || left_type == TYSIGNEDCHAR) && (right_type == TYFLOAT || right_type == TYDOUBLE))
        {
            error("Illegal conversion");
            return make_error_expr();
        }
        else if((right_type == TYUNSIGNEDCHAR || right_type == TYSIGNEDCHAR) && (left_type == TYFLOAT || left_type == TYDOUBLE))
        {
            error("Illegal conversion");
            return make_error_expr();
        }
        else if(left_type != TYSIGNEDCHAR && right->tag == BINOP)
        {
            if(right->u.binop.op== LESS_OP || right->u.binop.op == EQ_OP || right->u.binop.op == NE_OP || right->u.binop.op == GE_OP
                    || right->u.binop.op == GREATER_OP || right->u.binop.op == LE_OP)
            {
                error("Illegal conversion");
                return make_error_expr();
            }
        }
    }

    if (is_lval(left) == TRUE)
    {
        if (op != ASSIGN_OP)
        {
            EXPR derefNode = make_un_expr(DEREF_OP, left);
            ret->u.binop.left = derefNode;
        }
    }

    if (is_lval(right) == TRUE)
    {
        EXPR derefNode = make_un_expr(DEREF_OP, right);
        ret->u.binop.right = derefNode;
    }

    left_type = ty_query(left->type);
    right_type = ty_query(right->type);
    if (is_lval(left) == FALSE)
    {
        if (left_type == TYFLOAT && right_type == TYDOUBLE)
        {
            EXPR convertedNode = make_un_expr(CONVERT_OP, left);
            convertedNode->type = ty_build_basic(TYDOUBLE);
            ret->u.binop.left = convertedNode;
        }
        else if (right_type == TYFLOAT && left_type == TYDOUBLE)
        {
            EXPR convertedNode = make_un_expr(CONVERT_OP, right);
            convertedNode->type = ty_build_basic(TYDOUBLE);
            ret->u.binop.right = convertedNode;
        }
        else if (left_type == TYSUBRANGE)
        {
            EXPR convertedNode = make_un_expr(CONVERT_OP, left);
            base_type = ty_query_subrange(left->type, &low, &high);
            convertedNode->type = base_type;
            ret->u.binop.left = convertedNode;
        }
        else if (right_type == TYSUBRANGE)
        {
            EXPR convertedNode = make_un_expr(CONVERT_OP, right);
            base_type = ty_query_subrange(right->type, &low, &high);
            convertedNode->type = base_type;
            ret->u.binop.right = convertedNode;
        }
    }

    left_type = ty_query(ret->u.binop.left->type);
    right_type = ty_query(ret->u.binop.right->type);

    if (left_type == TYSIGNEDLONGINT && right_type == TYDOUBLE)
    {
        EXPR convertedNode = make_un_expr(CONVERT_OP, left);
        convertedNode->type = ty_build_basic(TYDOUBLE);
        ret->u.binop.left = convertedNode;
    }
    else if (right_type == TYSIGNEDLONGINT && left_type == TYDOUBLE)
    {
        EXPR convertedNode = make_un_expr(CONVERT_OP, right);
        convertedNode->type = ty_build_basic(TYDOUBLE);
        ret->u.binop.right = convertedNode;
    }

    left_type = ty_query(ret->u.binop.left->type);
    right_type = ty_query(ret->u.binop.right->type);
    if(op == ADD_OP || op == SUB_OP || op == MUL_OP || op == REALDIV_OP)
    {
        if(left_type == TYFLOAT)
        {
            left_type = TYDOUBLE;
            EXPR convertedNode = make_un_expr(CONVERT_OP, left);
            convertedNode->type = ty_build_basic(TYDOUBLE);
            ret->u.binop.left = convertedNode;
            if(right_type == TYSIGNEDLONGINT)
            {
                EXPR convertedNode = make_un_expr(CONVERT_OP, right);
                convertedNode->type = ty_build_basic(TYDOUBLE);
                ret->u.binop.right = convertedNode;
            }
        }
        if(right_type == TYFLOAT)
        {
            right_type = TYDOUBLE;
            EXPR convertedNode = make_un_expr(CONVERT_OP, right);
            convertedNode->type = ty_build_basic(TYDOUBLE);
            ret->u.binop.right = convertedNode;
            if(left_type == TYSIGNEDLONGINT)
            {
                EXPR convertedNode = make_un_expr(CONVERT_OP, left);
                convertedNode->type = ty_build_basic(TYDOUBLE);
                ret->u.binop.left = convertedNode;
            }
        }

        if ((right_type != TYDOUBLE && right_type != TYSIGNEDLONGINT) || (left_type != TYDOUBLE && left_type != TYSIGNEDLONGINT))
        {
            error("Nonnumerical type argument(s) to arithmetic operation");
            return make_error_expr();
        }
        else if (right_type == TYSIGNEDLONGINT && left_type == TYSIGNEDLONGINT)
        {
            ret->type = ty_build_basic(TYSIGNEDLONGINT);
        }
        else
        {
            ret->type = ty_build_basic(TYDOUBLE);
        }

    }
    else if (op == MOD_OP)
    {
        if ((right_type != TYDOUBLE && right_type != TYSIGNEDLONGINT) || (left_type != TYDOUBLE && left_type != TYSIGNEDLONGINT))
        {
            error("Nonnumerical type argument(s) to arithmetic operation");
            return make_error_expr();
        }
        else if (right_type == TYSIGNEDLONGINT && left_type == TYSIGNEDLONGINT)
        {
            ret->type = ty_build_basic(TYSIGNEDLONGINT);
        }
        else
        {
            ret->type = ty_build_basic(TYDOUBLE);
        }
    }
    else if (op ==DIV_OP)
    {
        ret->type = ty_build_basic(TYSIGNEDLONGINT);
    }
    else if (op== LESS_OP || op == EQ_OP || op == NE_OP || op == GE_OP || op == GREATER_OP || op == LE_OP)
    {
        if(right_type != left_type)
        {
            if(right_type != TYSIGNEDCHAR && left_type == TYSIGNEDLONGINT)
            {
                error("Illegal conversion");
                return make_error_expr();
            }
            else if(left_type != TYSIGNEDCHAR && right_type == TYSIGNEDLONGINT)
            {
                error("Illegal conversion");
                return make_error_expr();
            }

        }

        if (right_type == TYSIGNEDCHAR || right_type == TYUNSIGNEDCHAR)
        {
            EXPR convertedNode = make_un_expr(CONVERT_OP, right);
            convertedNode->type = ty_build_basic(TYSIGNEDLONGINT);
            ret->u.binop.right = convertedNode;
        }
        if (left_type == TYSIGNEDCHAR || left_type == TYUNSIGNEDCHAR)
        {
            EXPR convertedNode = make_un_expr(CONVERT_OP, left);
            convertedNode->type = ty_build_basic(TYSIGNEDLONGINT);
            ret->u.binop.left = convertedNode;
        }
        ret->type = ty_build_basic(TYSIGNEDLONGINT);
        EXPR convertedNode = make_un_expr(CONVERT_OP, ret);
        convertedNode->type = ty_build_basic(TYSIGNEDCHAR);
        ret = convertedNode;
    }

    return ret;
}


EXPR make_funcCall_expr(EXPR func, EXPR_LIST args)
{
    PARAM_LIST param;
    TYPE ret_type;
    TYPETAG expr_type;
    EXPR_LIST args_copy = args;

    if (ty_query(func->type) != TYFUNC)
    {
        error("not functiontype");
        return make_error_expr();
    }

    BOOLEAN b;
    ret_type = ty_query_func(func->type, &param, &b);

    if (!b)
    {
        while (args_copy != NULL)
        {

            if (is_lval(args_copy->expr))
            {
                EXPR derefNode = make_un_expr(DEREF_OP, args_copy->expr);
                args_copy->expr = derefNode; //expr now points to deref node
            }

            expr_type = ty_query(args_copy->expr->type);
            args_copy = args_copy->next;
        }
    }
    else
    {
        while (args_copy != NULL && param != NULL)
        {
            if (param->is_ref == TRUE)
            {
                if(ty_test_equality(args_copy->expr->type, param->type) == FALSE)
                {
                    error("types not equal");
                    return make_error_expr();
                }
            }
            else
            {
                if (is_lval(args_copy->expr) == TRUE)
                {
                    EXPR derefNode = make_un_expr(DEREF_OP, args_copy->expr);
                    args_copy->expr = derefNode;
                }

                expr_type = ty_query(args_copy->expr->type);
                if (expr_type == TYSIGNEDCHAR || expr_type == TYUNSIGNEDCHAR)
                {
                    EXPR convertedNode = make_un_expr(CONVERT_OP, args_copy->expr);
                    convertedNode->type = ty_build_basic(TYSIGNEDLONGINT);
                    args_copy->expr = convertedNode;
                }
                else if (expr_type == TYFLOAT)
                {
                    EXPR convertedNode = make_un_expr(CONVERT_OP, args_copy->expr);
                    convertedNode->type = ty_build_basic(TYDOUBLE);
                    args_copy->expr = convertedNode;
                }
            }

            args_copy = args_copy->next;
            param = param->next;
        }
    }

    EXPR ret;
    ret = (EXPR)malloc(sizeof(EXPR_NODE));
    assert(ret != NULL);
    ret->tag = FCALL;
    ret->type = ret_type;
    ret->u.fcall.args = args;
    ret->u.fcall.function = func;

    return ret;
}


void create_function_header(ST_ID id, TYPE ret_type, DIRECTIVE dir)
{
    PARAM_LIST params = NULL;
    BOOLEAN check_args;

    ST_DR rec = stdr_alloc();

    rec->tag = GDECL;
    rec->u.decl.is_ref = FALSE;
    rec->u.decl.v.global_func_name = st_get_id_str(id);

    if (dir == DIR_EXTERNAL)
    {
        rec->u.decl.sc = EXTERN_SC;
        rec->u.decl.type = ty_build_func(ret_type, params, FALSE);
    }
    else if (dir == DIR_FORWARD)
    {
        rec->u.decl.sc = NO_SC;
        rec->u.decl.type = ty_build_func(ret_type, params, TRUE);
    }
    else
    {
        error("Wrong place: in create_function_header");
    }

    if (!st_install(id, rec))
    {
        error("Duplicate forward or external function declaration");
        free(rec);
    }

}

int prework_function_body(ST_ID id, TYPE ret_type)
{
    PARAM_LIST params = NULL;
    BOOLEAN check_args;
    TYPE ret_type1;
    int block;
    int init_offset;

    /* Call st_lookup to see if id is previously installed in current block */
    ST_DR rec = st_lookup(id, &block);
    if (rec == NULL)
    {
        rec = stdr_alloc();
        rec->tag = FDECL;
        rec->u.decl.type = ty_build_func(ret_type, params, FALSE);
        rec->u.decl.sc = NO_SC;
        rec->u.decl.is_ref = FALSE;
        rec->u.decl.v.global_func_name = st_get_id_str(id);

        if (!st_install(id, rec))
        {
            error("Couldn't install into symbol table inside of enter_func()");
            free(rec);
        }
    }
    else
    {
        if (rec->tag != GDECL || rec->u.decl.sc != NO_SC)
        {
            error("Error in enter_func(), no GDECL or no NO_SC");
            return;
        }
        else
        {
            ret_type1 = ty_query_func(rec->u.decl.type, &params, &check_args);
            if(params != NULL)
            {
                error("params not NULL");
            }
            if (ty_test_equality(ret_type, ret_type1) == TRUE)
            {
                rec->tag = FDECL;
            }
            else
            {
                error("Error in enter_func(), return types not equal");
            }
        }
    }

    fi_top++;
    func_id_stack[fi_top] = id;

    st_enter_block();
    b_init_formal_param_offset();

    install_params(params);

    init_offset = b_get_local_var_offset();
    if (rec->u.decl.type == TYVOID)
    {
        init_offset -= 8;
    }
    return init_offset;
}

void enter_func_body(ST_ID id, TYPE ret_type, int loc_var_offset)
{
    TYPE ret_type1;
    TYPE param_type;
    PARAM_LIST params;
    BOOLEAN check;
    TYPETAG tag;
    TYPETAG param_tag;

    int block;
    long low,high;

    b_func_prologue(st_get_id_str(id));

    ST_DR rec = st_lookup(id, &block);
    ret_type1 = ty_query_func(rec->u.decl.type, &params, &check);
    tag = ty_query(ret_type);


    while (params != NULL)
    {
        param_tag = ty_query(params->type);
        rec = st_lookup(params->id, &block);

        if (param_tag == TYSUBRANGE)
        {
            param_type = ty_query_subrange(ret_type1, &low, &high);
            b_store_formal_param(ty_query(param_type));
        }
        else if (rec->u.decl.is_ref == TRUE)
        {
            b_store_formal_param(TYPTR);
        }
        else
        {
            b_store_formal_param(param_tag);
        }

        params = params->next;
    }
    if (tag != TYVOID)
    {
        b_alloc_return_value();
    }

    b_alloc_local_vars(loc_var_offset);
}

void exit_func_body(ST_ID id, TYPE ret_type)
{
    TYPETAG tag;
    long low, high;

    tag = ty_query(ret_type);

    fi_top--;

    if (tag == TYSUBRANGE)
    {
        b_prepare_return(ty_query(ty_query_subrange(ret_type, &low, &high)));
    }
    else
    {
        b_prepare_return(tag);
    }

    b_func_epilogue(st_get_id_str(id));
    st_exit_block();
}


void install_params(PARAM_LIST list)
{
    long low, high;

    while (list != NULL)
    {
        ST_DR rec = stdr_alloc();

        rec->tag = PDECL;
        rec->u.decl.sc = list->sc;
        rec->u.decl.is_ref = list->is_ref;
        rec->u.decl.err = list->err;

        if (ty_query(list->type) == TYSUBRANGE)
        {
            rec->u.decl.type = ty_query_subrange(list->type, &low, &high);
        }
        else
        {
            rec->u.decl.type = list->type;
        }

        if (list->is_ref == TRUE)
        {
            rec->u.decl.v.offset = b_get_formal_param_offset(TYPTR);
        }
        else
        {
            rec->u.decl.v.offset = b_get_formal_param_offset(rec->u.decl.type);
        }

        st_install(list->id, rec);
        list = list->next;
    }
}


EXPR handle_assign_procCall(EXPR lhs, ST_ID id, EXPR rhs)
{
    PARAM_LIST params;
    BOOLEAN check;

    if(rhs == NULL)
    {
        if (lhs->tag == UNOP)
        {
            if (lhs->u.unop.op == NEW_OP || lhs->u.unop.op == DISPOSE_OP)
            {
                return lhs;
            }
        }

        if (lhs->tag == GID || lhs->tag == LFUN)
        {
            if (ty_query(lhs->type) == TYFUNC)
            {
                EXPR ret = make_funcCall_expr(lhs, NULL);
                return ret;
            }
            else
            {
                error("Expected procedure name, saw data");
                return make_error_expr();
            }
        }
        else if (lhs->tag == FCALL)
        {
            if (ty_query(lhs->type) == TYVOID)
            {
                return lhs;
            }
            else
            {
                error("Procedure call to a nonvoid type");
                return make_error_expr();
            }
        }
        else if (lhs->tag == ERROR)
        {
            return make_error_expr();
        }
        else
        {
            error("Procedure call expected");
            return make_error_expr();
        }
    }
    else
    {
        if (id == func_id_stack[fi_top] && fi_top >= 0)
        {
            if (ty_query(ty_query_func(lhs->type, &params, &check)) != TYVOID)
            {
                EXPR ret = make_un_expr(SET_RETURN_OP, rhs);
                ret->type = ty_query_func(lhs->type, &params, &check);
                return ret;
            }
            else
            {
                error("Cannot set the return value of a procedure");
                return make_error_expr();
            }
        }
        else
        {
            EXPR ret = make_bin_expr(ASSIGN_OP, lhs, rhs);
            return ret;
        }
    }
}

EXPR make_error_expr()
{
    EXPR ret;
    ret = (EXPR)malloc(sizeof(EXPR_NODE));
    assert(ret != NULL);
    ret->tag = ERROR;
    ret->type = ty_build_basic(TYERROR);
    return ret;
}

BOOLEAN is_lval(EXPR expr)
{
    if (expr->tag == LVAR)
    {
        return TRUE;
    }
    else if (expr->tag == GID)
    {
        if (ty_query(expr->type) == TYFUNC || ty_query(expr->type) == TYERROR)
        {
            return FALSE;
        }
        else
        {
            return TRUE;
        }
    }
    else if (expr->tag == UNOP)
    {
        if (expr->u.unop.op == INDIR_OP)
        {
            return TRUE;
        }
    }
	else if (expr->tag == ARRAY_ACCESS)
	{
		return TRUE;
	}
    else
    {
        return FALSE;
    }
}




/**** ----------------  START OF CODE: PART 3  ------------------------ ****/

unsigned long get_value_range(TYPE type, long * low)
{
    return -9999;
}


EXPR make_array_access_expr(EXPR array, EXPR_LIST indices)
{

    if (ty_query(array->type) != TYARRAY)
    {
        error("Nonarray in array access expression");
        return make_error_expr();
    }

    TYPE array_type;
    INDEX_LIST i;
    EXPR_LIST test = indices;

    array_type = ty_query_array(array->type, &i);

    while (indices != NULL && i != NULL)
    {

        if (is_lval(indices->expr) == TRUE)
        {
            indices->expr = make_un_expr(DEREF_OP, indices->expr);
        }

        if (ty_query(indices->expr->type) != TYSIGNEDLONGINT)
        {
            error("Incompatible index type in array access");
            return make_error_expr();
        }

        i = i->next;
        indices = indices->next;
    }

    if ((indices == NULL && i != NULL) || (indices != NULL && i == NULL))
    {
        error("Wrong number of indices in array access");
        return make_error_expr();
    }

    EXPR ret;
    ret = (EXPR)malloc(sizeof(EXPR_NODE));
    assert(ret != NULL);
    ret->tag = ARRAY_ACCESS;
    ret->type = array_type;
    ret->u.fcall.args = test;
    ret->u.fcall.function = array;

    return ret;

}

VAL_LIST new_case_value(TYPETAG type, long lo, long hi)
{
    VAL_LIST new_val_list;
    new_val_list = (VAL_LIST)malloc(sizeof(VAL_LIST_REC));
    assert(new_val_list != NULL);
    new_val_list->lo = lo;
    new_val_list->hi = hi;
    new_val_list->type = type;
    return new_val_list;
}

BOOLEAN check_case_values(TYPETAG type, VAL_LIST vals, VAL_LIST prev_vals)
{

    VAL_LIST vals_copy = vals;
    VAL_LIST pvals_copy = prev_vals;


    if(prev_vals == NULL)
    {

        return TRUE;
    }

    while(vals_copy != NULL)
    {
        if(type != vals->type)
        {
            error("Case constant type does not match case expression type");
            return FALSE;
        }

        if(vals->lo != vals->hi)
            if(vals->lo >= vals->hi)
                warning("Empty range in case constant (ignored)");


        while(pvals_copy != NULL)
        {

            if(pvals_copy->lo != pvals_copy->hi)
            {
                if(vals_copy->lo != vals_copy->hi)
                {
                    if((vals_copy->lo >= pvals_copy->lo && vals_copy->lo <= pvals_copy->hi) || (vals_copy->hi >= pvals_copy->lo && vals_copy->hi <= pvals_copy->hi))

                    {
                        error("Overlapping constants in case statement");
                        return FALSE;
                    }
                }
                else
                {
                    if(vals_copy->lo >= pvals_copy->lo && vals_copy->lo <= pvals_copy->hi)
                    {
                        /*Error, return false*/
                        error("Overlapping constants in case statement");
                        return FALSE;
                    }
                }
            }
            else
            {

                if(vals_copy->lo != vals_copy->hi)
                {
                    /*Checks to see if inside subrange*/
                    if(pvals_copy->lo >= vals_copy->lo && pvals_copy->lo <= vals_copy->hi)
                    {
                        /*Error, return false*/
                        error("Overlapping constants in case statement");
                        return FALSE;
                    }
                }
                else
                {
                    /*Checks to see if same*/
                    if(vals_copy->lo == pvals_copy->lo)
                    {
                        /*Error, return false*/
                        error("Overlapping constants in case statement");
                        return FALSE;
                    }
                }
            }
            /*If last item all checks passed add to end of list*/
            if(pvals_copy->next == NULL)
            {
                /*Adds to list*/
                pvals_copy->next = new_case_value(vals_copy->type, vals_copy->lo, vals_copy->hi);
                pvals_copy = NULL;
            }
            else
            {
                /*Moves onto the next item*/
                pvals_copy = pvals_copy->next;
            }
        }
        /*Moves onto the next item*/
        vals_copy = vals_copy->next;
        pvals_copy = prev_vals;
    }

    /*All checks passed return true*/
    return TRUE;

}

void case_value_list_free(VAL_LIST vals)
{
    if(vals->next != NULL)
        case_value_list_free(vals->next);
    free(vals);
}

BOOLEAN get_case_value(EXPR expr, long * val, TYPETAG * type)
{

    if(expr->tag == INTCONST)
    {
        *type = ty_query(expr->type);
        *val = expr->u.intval;
        return TRUE;
    }
    else if(expr->tag == STRCONST)
    {
        if(strlen(expr->u.strval) == 1)
        {
            *type = TYUNSIGNEDCHAR;
            *val = expr->u.strval[0];
            expr = make_intconst_expr(expr->u.strval[0], ty_build_basic(TYSIGNEDLONGINT));
            return TRUE;
        }
        else
        {
            error("STRCONST not length 1");
            return FALSE;
        }
    }
    else if(expr->tag == UNOP)
    {
        if(expr->u.unop.op == CHR_OP)
        {
            *type = TYUNSIGNEDCHAR;
            *val = expr->u.unop.operand->u.intval;
            expr = make_intconst_expr((int)expr->u.unop.operand->u.intval, ty_build_basic(TYSIGNEDLONGINT));
            return TRUE;
        }
        else if(expr->u.unop.op == ORD_OP)
        {
            error("in ord op");
        }
    }
    else
    {
        error("Case constant has non-ordinal type");
        return FALSE;
    }
}


BOOLEAN check_for_preamble(EXPR var, EXPR init, EXPR limit)
{
    return TRUE;
}

BOOLEAN check_case_const(VAL_LIST list, TYPETAG tag)
{

    if(list != NULL)
    {

        VAL_LIST copy = list;

        while(copy != NULL)
        {
            if(list->type != tag)
            {
                error("Case constant type does not match case expression type");
                return FALSE;
            }
            copy = copy->next;
        }
    }
    return TRUE;
}

