#ifndef TREE_C
#define TREE_C
#include "types.h"
#include "symtab.h"
#include "message.h"

typedef struct var_id
{
    ST_ID id;
    struct var_id *next;
} VAR_ID, *VAR_ID_LIST;


/* Records the current function identifier to detect return value assigns */
extern ST_ID func_id_stack[BS_DEPTH];
extern int fi_top;
extern int stack_counter;
extern char* exit_case_labels[100];
extern int exit_case_top;


typedef enum { DIR_EXTERNAL, DIR_FORWARD } DIRECTIVE;

typedef enum
{
    INTCONST, REALCONST, STRCONST, GID, LVAR, LFUN, NULLOP, UNOP, BINOP,
    FCALL, ERROR, ARRAY_ACCESS
} EXPR_TAG;

typedef enum
{
    ADD_OP, SUB_OP, MUL_OP, DIV_OP, MOD_OP, REALDIV_OP, EQ_OP, LESS_OP, LE_OP,
    NE_OP, GE_OP, GREATER_OP, SYMDIFF_OP, OR_OP, XOR_OP, AND_OP, BIN_SUCC_OP,
    BIN_PRED_OP, ASSIGN_OP
} EXPR_BINOP;

typedef enum
{
    CONVERT_OP, DEREF_OP, NEG_OP, PLUS_OP, ORD_OP, CHR_OP, SUCC_OP, PRED_OP,
    NEW_OP, DISPOSE_OP, NOT_OP, ABS_OP, SQR_OP, SIN_OP, COS_OP, EXP_OP, LN_OP, SQRT_OP, ARCTAN_OP,
    ARG_OP, TRUNC_OP, ROUND_OP, CARD_OP, ODD_OP, EMPTY_OP, POSITION_OP,
    LASTPOSITION_OP, LENGTH_OP, TRIM_OP, BINDING_OP, DATE_OP, TIME_OP,
    UN_EOF_OP, UN_EOLN_OP, INDIR_OP, ADDRESS_OP,
    SET_RETURN_OP
} EXPR_UNOP;



typedef enum
{
    NULL_EOF_OP, NULL_EOLN_OP, NIL_OP
} EXPR_NULLOP;

typedef enum 
{ 
	FOR_DIR_UP, FOR_DIR_DOWN 
} FOR_DIR;


/* Node to store id and return type for function_heading production in gram.y */
typedef struct func_heading
{
    ST_ID	id;
    TYPE	ret_type;
} FUNC_HEADING;

typedef struct
{
    struct exprnode * expr;
    ST_ID id;
} EXPR_ID;

/* Used for lists of actual arguments to functions/procedures */
typedef struct exprlistnode
{
    struct exprnode * expr;
    struct exprlistnode * next;
} EXPR_LIST_NODE, * EXPR_LIST;

/* The syntax tree node for an expression */
typedef struct exprnode
{
    EXPR_TAG tag;
    TYPE type;
    union
    {
        long intval;
        double realval;
        char * strval;
        ST_ID gid;	    	/* For global variables and global functions */
        struct
        {
            BOOLEAN is_ref;
            int offset;
            int link_count;
        } lvar;
        struct
        {
            char * global_name;
            int link_count;
        } lfun;
        struct
        {
            EXPR_NULLOP op;
        } nullop;
        struct
        {
            EXPR_UNOP op;
            struct exprnode * operand;
        } unop;
        struct
        {
            EXPR_BINOP op;
            struct exprnode * left, * right;
        } binop;
        struct
        {
            struct exprnode * function;
            EXPR_LIST args;
        } fcall;
    } u;
} EXPR_NODE, *EXPR;



typedef struct val_node
{
    long lo, hi;
    TYPETAG type;
    struct val_node * next;
} VAL_LIST_REC, *VAL_LIST;


typedef struct
{
    TYPETAG type;
    char * label;
    VAL_LIST values;
} CASE_REC;



void create_gdecl(VAR_ID_LIST list,TYPE type);
void create_typename(ST_ID id,TYPE new_type);

TYPE check_typename( ST_ID id );
TYPE check_function_type(TYPE t);
TYPE check_array(TYPE array, INDEX_LIST i);
PARAM_LIST check_param(PARAM_LIST p);

VAR_ID_LIST create_variable_list (VAR_ID_LIST list,ST_ID id);
PARAM_LIST create_param_list(VAR_ID_LIST id_list,TYPE type,BOOLEAN value);

PARAM_LIST concat_param_list (PARAM_LIST list1,PARAM_LIST list2);
INDEX_LIST concat_index_lists (INDEX_LIST list1,TYPE type);

INDEX_LIST create_list_from_type(TYPE type);

TYPE check_subrange(EXPR lo, EXPR hi);

TYPE build_unresolved_pointer(TYPE ret_type, TYPE object);
void resolve_all_ptr();

/* start of definitions: part 2 */
void enter_main_body();
void exit_main_body();

void create_function_header(ST_ID id, TYPE type, DIRECTIVE dir);
int prework_function_body(ST_ID id, TYPE ret_type);
void enter_func_body(ST_ID id, TYPE ret_type, int loc_var_offset);
void exit_func_body(ST_ID id, TYPE ret_type);
void install_params(PARAM_LIST list);

EXPR_LIST expr_list_reverse(EXPR_LIST list);
EXPR_LIST expr_prepend(EXPR expr, EXPR_LIST list);
void expr_free(EXPR expr);
void expr_list_free(EXPR_LIST list);
EXPR make_intconst_expr(long val, TYPE type);
EXPR make_realconst_expr(double val);
EXPR make_strconst_expr(char * str);
EXPR make_id_expr(ST_ID id);
EXPR make_null_expr(EXPR_NULLOP op);
EXPR make_un_expr(EXPR_UNOP op, EXPR sub);
EXPR make_bin_expr(EXPR_BINOP op, EXPR left, EXPR right);
EXPR make_funcCall_expr(EXPR func, EXPR_LIST args);
EXPR make_error_expr();
EXPR handle_assign_procCall(EXPR lhs, ST_ID id, EXPR rhs);
BOOLEAN is_lval(EXPR expr);


/**** ----------------  START OF CODE: PART 3  ------------------------ ****/

unsigned long get_value_range(TYPE type, long * low);



EXPR make_array_access_expr(EXPR array, EXPR_LIST indices);


VAL_LIST new_case_value(TYPETAG type, long lo, long hi);


BOOLEAN check_case_values(TYPETAG type, VAL_LIST vals, VAL_LIST prev_vals);



void case_value_list_free(VAL_LIST vals);


BOOLEAN get_case_value(EXPR expr, long * val, TYPETAG * type);


BOOLEAN check_for_preamble(EXPR var, EXPR init, EXPR limit);

#endif
