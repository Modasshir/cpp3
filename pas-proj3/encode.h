#ifndef ENCODE_H
#define ENCODE_H

#include "defs.h"
#include "types.h"
#include "message.h"
#include "tree.h"


extern char* exit_labels[100];
extern int exit_label_curr;



void decl_var_list(VAR_ID_LIST list_id, TYPE type);

unsigned int get_size(TYPE type);

int get_align(TYPE type);



void encode_expr(EXPR expr);

void encode_unop(EXPR_UNOP op, EXPR expr);

void encode_binop(EXPR_BINOP out, EXPR expr);

void encode_funcCall(EXPR func, EXPR_LIST args);


void new_exit_label();
char* current_exit_label();
char* old_exit_label();

void new_exit_label();


char * old_exit_label();

char * current_exit_label();


BOOLEAN is_exit_label();

void encode_loopCounter(EXPR expr);
void encode_dispatch(VAL_LIST vals, char * label);

char * encode_for_preamble(EXPR var, EXPR init, int dir, EXPR limit);
void encode_arrayaccess(EXPR expr, EXPR_LIST indices);
#endif
