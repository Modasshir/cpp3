#include <stdlib.h>
#include "defs.h"
#include "types.h"
#include "symtab.h"
#include "message.h"
#include "backend-x86.h"
#include "encode.h"

int exit_label = -1;
char* exit_label_stack[100];

void decl_var_list(VAR_ID_LIST list_id, TYPE type)
{
    if (ty_query(type) == TYERROR || ty_query(type) == TYFUNC)
        return;

    while(list_id)
    {
        ST_ID st_id = list_id->id;
        /* Get the id from the Symbol Table */
        char *id = st_get_id_str(st_id);
        unsigned int size = get_size(type);
        int alignment = get_align(type);

        if (size != 0 && alignment != 0)
        {
            b_global_decl(id, alignment, size);
            b_skip(size);
        }
        else
        {
            bug("Error in decl_var_list");
        }
        list_id = list_id->next;
    }
}

unsigned int get_size(TYPE type)
{

    TYPETAG tag;
    tag = ty_query(type);

    INDEX_LIST indices;
    unsigned int array_size;
    TYPE temp_type = type;

    /* For subrange */
    long low, high;
    //pascal int and longint has 4 bytes
    switch(tag)
    {
    case TYUNSIGNEDCHAR:
        return 1;
    case TYSIGNEDCHAR:
        return 1;
    case TYUNSIGNEDSHORTINT:
        return 2;
    case TYSIGNEDSHORTINT:
        return 2;
    case TYUNSIGNEDINT:
        return 4;
    case TYSIGNEDINT:
        return 4;
    case TYUNSIGNEDLONGINT:
        return 4;
    case TYSIGNEDLONGINT:
        return 4;
    case TYFLOAT:
        return 4;
    case TYDOUBLE:
        return 8;
    case TYLONGDOUBLE:
        return 8;
    case TYPTR:
        return 4;//pointer is 4 bytes
    case TYARRAY:
        array_size = get_size(ty_query_array(type, &indices));
        while (indices)
        {
            ty_query_subrange(indices->type,&low,&high);
            array_size *= high - low + 1;
            indices = indices->next;
        }
        return array_size;
    case TYSUBRANGE:
        return get_size(ty_query_subrange(type, &low, &high));
    default:
        bug("Illegal type tag %d in get_size", tag);
        return 0;
    }
}



int get_align(TYPE type)
{

    TYPETAG tag;
    tag = ty_query(type);

    INDEX_LIST indices;

    long low, high;

    switch(tag)
    {
    case TYUNSIGNEDCHAR:
        return 1;
    case TYSIGNEDCHAR:
        return 1;
    case TYUNSIGNEDSHORTINT:
        return 2;
    case TYSIGNEDSHORTINT:
        return 2;
    case TYUNSIGNEDINT:
        return 4;
    case TYSIGNEDINT:
        return 4;
    case TYUNSIGNEDLONGINT:
        return 4;
    case TYSIGNEDLONGINT:
        return 4;
    case TYFLOAT:
        return 4;
    case TYDOUBLE:
        return 8;
    case TYLONGDOUBLE:
        return 8;
    case TYPTR:
        return 4;
    case TYARRAY:
        return get_align(ty_query_array(type, &indices));
    case TYSUBRANGE:
        return get_align(ty_query_subrange(type, &low, &high));
    default:
        bug("Illegal type tag %d in get_align", tag);
        return 0;
    }
}




/**** ----------------  START OF CODE: PART 2  ------------------------ ****/

void encode_expr(EXPR expr)
{
    if (expr != NULL)
    {
        switch (expr->tag)
        {
        case LFUN:
        case ERROR:
            break;
        case INTCONST:
            b_push_const_int(expr->u.intval);
            if(ty_query(expr->type) == TYUNSIGNEDCHAR)
                b_convert(TYSIGNEDLONGINT, TYUNSIGNEDCHAR);
            if(ty_query(expr->type) == TYSIGNEDCHAR)
                b_convert(TYSIGNEDLONGINT, TYSIGNEDCHAR);
            break;
        case REALCONST:
            b_push_const_double(expr->u.realval);
            break;
        case STRCONST:
            b_push_const_string(expr->u.strval);
            break;
        case GID:
            b_push_ext_addr(st_get_id_str(expr->u.gid));
            break;
        case LVAR:
            b_push_loc_addr(0);
            int i = 0;
            for (i = 0; i < expr->u.lvar.link_count; i++)
            {
                b_offset(FUNC_LINK_OFFSET);
                b_deref(TYPTR);
            }
            b_offset(expr->u.lvar.offset);
            if (expr->u.lvar.is_ref == TRUE)
                b_deref(TYPTR);
            break;
        case NULLOP:
            b_push_const_int(0);
            break;
        case UNOP:
            encode_unop(expr->u.unop.op, expr);
            break;
        case BINOP:
            encode_binop(expr->u.binop.op, expr);
            break;
        case FCALL:
            encode_funcCall(expr->u.fcall.function, expr->u.fcall.args);
            break;
        case ARRAY_ACCESS:
            encode_arrayaccess(expr->u.fcall.function, expr->u.fcall.args);
            break;
        }
    }
    else
    {
        bug("Expression is null ");
    }
}

void encode_arrayaccess(EXPR expr, EXPR_LIST indices)
{
    long low_index, high_index, range;
    int align;
    unsigned int size;
    TYPE array_type, subrange_type;
    INDEX_LIST index_list;
    EXPR_LIST rev_indices;

    encode_expr(expr);
    array_type = ty_query_array(expr->type, &index_list);
    align = get_align(array_type);
    size = get_size(array_type);

    rev_indices = expr_list_reverse(indices);

    while (rev_indices != NULL && index_list != NULL)
    {
        encode_expr(rev_indices->expr);
        subrange_type = ty_query_subrange(index_list->type, &low_index, &high_index);
        range = high_index - low_index + 1;

        if (index_list->next == NULL)
        {
            b_push_const_int(low_index);
            b_arith_rel_op(B_SUB,TYSIGNEDLONGINT);
            b_ptr_arith_op(B_ADD, TYSIGNEDLONGINT, size);
        }
        //else if (subrange do later)
        rev_indices = rev_indices->next;
        index_list = index_list->next;
    }
}
void encode_unop(EXPR_UNOP op, EXPR expr)
{
    long low, high;
    TYPE baseT;
    TYPETAG tag, rval_tag;
    ST_ID id;
    TYPE type, base_type;
    BOOLEAN converted_to_int;

    if(op == DISPOSE_OP)
        b_alloc_arglist(4);

    encode_expr(expr->u.unop.operand);
    converted_to_int = FALSE;
    tag = ty_query(expr->u.unop.operand->type);
    rval_tag = ty_query(expr->type);


    switch(op)
    {
    case INDIR_OP:
    case PLUS_OP:
        break;
    case CONVERT_OP:
        if(tag==TYSUBRANGE)
        {
            base_type = ty_query_subrange(expr->u.unop.operand->type, &low, &high);
            b_convert(TYSUBRANGE, ty_query(base_type));
        }
        else if(tag != TYPTR)
        {
            b_convert(tag, rval_tag);
        }
        break;
    case NEG_OP:
        b_negate(tag);
        break;
    case ORD_OP:
        if(tag==TYUNSIGNEDCHAR)
        {
            b_convert(tag, TYSIGNEDLONGINT);
        }
        break;
    case CHR_OP:
        if(tag==TYSIGNEDLONGINT)
        {
            b_convert(tag, TYUNSIGNEDCHAR);
        }
        break;
    case SUCC_OP:
        if(tag!=TYSIGNEDLONGINT)
        {
            b_convert(tag,TYSIGNEDLONGINT);
            converted_to_int = TRUE;
        }
        b_push_const_int(1);
        b_arith_rel_op(B_ADD, TYSIGNEDLONGINT);
        if(converted_to_int == TRUE)
        {
            b_convert(TYSIGNEDLONGINT,tag);
        }
        break;
    case PRED_OP:
        if(tag != TYSIGNEDLONGINT)
        {
            b_convert(tag,TYSIGNEDLONGINT);
            converted_to_int = TRUE;
        }
        b_push_const_int(-1);
        b_arith_rel_op(B_ADD, TYSIGNEDLONGINT);
        if(converted_to_int == TRUE)
        {
            b_convert(TYSIGNEDLONGINT, tag);
        }
        break;
    case NEW_OP:
        b_alloc_arglist(4);
        b_push_const_int(get_size(ty_query_ptr(expr->u.unop.operand->type, &id)));
        b_load_arg(TYUNSIGNEDINT);
        b_funcall_by_name("malloc", TYPTR);
        b_assign(TYPTR);
        b_pop();
        break;
    case DISPOSE_OP:
        b_load_arg(TYPTR);
        b_funcall_by_name("free", TYVOID);
        break;
    case DEREF_OP:
        b_deref(tag);
        break;
    case SET_RETURN_OP:
        b_set_return(ty_query(expr->u.unop.operand->type));
        break;
    }
}

void encode_binop(EXPR_BINOP out, EXPR expr)
{
    TYPETAG type_tag;
    TYPETAG left_type_tag, right_type_tag;

    if(expr->u.binop.left->tag==INTCONST && expr->u.binop.right->tag==INTCONST)
    {
        EXPR ret;
        switch (out)
        {
        case ADD_OP:
            ret = make_intconst_expr((expr->u.binop.left->u.intval + expr->u.binop.right->u.intval), ty_build_basic(TYSIGNEDLONGINT));
            encode_expr(ret);
            break;
        case SUB_OP:
            ret = make_intconst_expr((expr->u.binop.left->u.intval - expr->u.binop.right->u.intval), ty_build_basic(TYSIGNEDLONGINT));
            encode_expr(ret);
            break;
        case MUL_OP:
            ret = make_intconst_expr((expr->u.binop.left->u.intval * expr->u.binop.right->u.intval), ty_build_basic(TYSIGNEDLONGINT));
            encode_expr(ret);
            break;
        case DIV_OP:
            ret = make_intconst_expr((expr->u.binop.left->u.intval / expr->u.binop.right->u.intval), ty_build_basic(TYSIGNEDLONGINT));
            encode_expr(ret);
            break;
        case MOD_OP:
            ret = make_intconst_expr((expr->u.binop.left->u.intval % expr->u.binop.right->u.intval), ty_build_basic(TYSIGNEDLONGINT));
            encode_expr(ret);
            break;
        case REALDIV_OP:
            break;
        }
    }
    else
    {
        encode_expr(expr->u.binop.left);
        encode_expr(expr->u.binop.right);

        type_tag = ty_query(expr->type);
        if(type_tag == TYFLOAT) type_tag = TYDOUBLE;

        left_type_tag = ty_query(expr->u.binop.left->type);
        right_type_tag = ty_query(expr->u.binop.right->type);

        switch (out)
        {
        case SYMDIFF_OP:
        case OR_OP:
        case XOR_OP:
        case AND_OP:
            break;
        case ADD_OP:
            b_arith_rel_op(B_ADD, type_tag);
            break;

        case SUB_OP:
            b_arith_rel_op(B_SUB, type_tag);
            break;

        case MUL_OP:
            b_arith_rel_op(B_MULT, type_tag);
            break;

        case DIV_OP:
            b_arith_rel_op(B_DIV, type_tag);
            break;

        case MOD_OP:
            b_arith_rel_op(B_MOD, type_tag);
            break;

        case REALDIV_OP:
            b_arith_rel_op(B_DIV, type_tag);
            break;

        case EQ_OP:
            b_arith_rel_op(B_EQ, type_tag);
            break;

        case LESS_OP:
            b_arith_rel_op(B_LT, type_tag);
            break;

        case LE_OP:
            b_arith_rel_op(B_LE, type_tag);
            break;

        case NE_OP:
            b_arith_rel_op(B_NE, type_tag);
            break;

        case GE_OP:
            b_arith_rel_op(B_GE, type_tag);
            break;

        case GREATER_OP:
            b_arith_rel_op(B_GT, type_tag);
            break;

        case ASSIGN_OP:
            if(expr->u.binop.left->tag == LVAR)
                b_push_loc_addr(expr->u.binop.left->u.lvar.offset);
            if(left_type_tag != right_type_tag)
            {
                if(expr->u.binop.right->tag==INTCONST)
                    b_convert(TYSIGNEDLONGINT, left_type_tag);
                else
                    b_convert(right_type_tag, left_type_tag);
            }
            b_assign(left_type_tag);
            b_pop();
            break;
        }
    }
}

void encode_funcCall(EXPR func, EXPR_LIST args)
{
    int arg_list_size;
    EXPR_LIST t_arg;
    char *func_gname;
    TYPE func_ret_type;
    TYPETAG arg_tag;
    PARAM_LIST func_params;
    BOOLEAN check_args;

    func_ret_type = ty_query_func(func->type, &func_params, &check_args);
    arg_list_size = 0;
    t_arg = args;

    if(func->tag == GID)
        func_gname = st_get_id_str(func->u.gid);
    t_arg = args;
    while(t_arg != NULL)
    {

        if(ty_query(t_arg->expr->type)==TYDOUBLE||ty_query(t_arg->expr->type)==TYFLOAT)
            arg_list_size += 8;
        else
            arg_list_size += 4;

        t_arg=t_arg->next;
    }

    b_alloc_arglist(arg_list_size);
    t_arg = args;
    while(t_arg != NULL)
    {

        encode_expr(t_arg->expr);
        arg_tag = ty_query(t_arg->expr->type);
        if(func_params != NULL)
        {
            if(func_params->is_ref==TRUE)
            {
                if(is_lval(t_arg->expr)==FALSE)
                    bug("Function argument expected to be lval in encode_funcCall");

                if(ty_test_equality(t_arg->expr->type, func_params->type)==FALSE)
                    error("Parameter types do not match");

                b_load_arg(TYPTR);
            }
            else
            {
                if(is_lval(t_arg->expr)==TRUE)
                    b_deref(arg_tag);
                if(arg_tag==TYSIGNEDCHAR||arg_tag==TYUNSIGNEDCHAR)
                {
                    b_convert(arg_tag,TYSIGNEDLONGINT);
                    b_load_arg(TYSIGNEDLONGINT);
                }
                else if(arg_tag==TYFLOAT)
                {
                    b_convert(arg_tag,TYDOUBLE);
                    b_load_arg(TYDOUBLE);
                }
                else
                    b_load_arg(arg_tag);
            }
        }
        else
        {

            if(arg_tag==TYSIGNEDCHAR||arg_tag==TYUNSIGNEDCHAR)
            {
                b_convert(arg_tag, TYSIGNEDLONGINT);
                b_load_arg(TYSIGNEDLONGINT);
            }
            else if(arg_tag==TYFLOAT)
            {
                b_convert(arg_tag,TYDOUBLE);
                b_load_arg(TYDOUBLE);
            }
            else
                b_load_arg(arg_tag);
        }

        t_arg=t_arg->next;

        if(func_params!=NULL)
            func_params=func_params->next;
    }
    b_funcall_by_name(func_gname,ty_query(func_ret_type));
}

/**** ----------------  START OF CODE: PART 3  ------------------------ ****/

void new_exit_label()
{

    char * new_label = new_symbol();
    exit_label_curr++;
    exit_labels[exit_label_curr] = new_label;
}

char * old_exit_label()
{
    if(exit_label_curr < 0)
    {
        bug("Exit label stack empty");
        return;
    }

    char * ret_label = exit_labels[exit_label_curr];
    exit_label_curr--;
    return ret_label;
}

char * current_exit_label()
{
    if(exit_label_curr < 0)
    {
        bug("Exit label stack empty");
        return;
    }

    char * ret_label = exit_labels[exit_label_curr];
    return ret_label;
}


BOOLEAN is_exit_label()
{
    if(exit_label_curr >= 0)
        return TRUE;
    else
        return FALSE;
}

void encode_loopCounter(EXPR expr)
{
	if(expr->tag == UNOP && expr->u.unop.op == DEREF_OP)
	{
		encode_expr(expr->u.unop.operand);		
	}
	else
	{
		encode_expr(expr);
	}
}


void encode_dispatch(VAL_LIST vals, char * label)
{
    char * match_label = new_symbol();

    if(vals != NULL)
        while(vals != NULL)
        {
            if(vals->lo == vals->hi)
            {
                b_dispatch(B_EQ, TYSIGNEDLONGINT, vals->lo, match_label, TRUE);
            }
            else
            {
                char *  new_label = new_symbol();
                b_dispatch(B_LT, TYSIGNEDLONGINT, vals->lo, new_label, FALSE);
                b_dispatch(B_LE, TYSIGNEDLONGINT, vals->hi, match_label, FALSE);
                b_label(new_label);
            }
            vals = vals->next;
        }

    b_jump(label);

    b_label(match_label);

}
char * encode_for_preamble(EXPR var, EXPR init, int dir, EXPR limit)
{
    return;
}


